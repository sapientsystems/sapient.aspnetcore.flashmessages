using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Sapient.AspNetCore.FlashMessages;
using Xunit;

namespace Sapient.AspNetCore.FlashMessages.Test.Unit
{
    public class FlashMessageStoreTest
    {
        private FlashMessage TestMessage1 = new FlashMessage
        {
            MessageType = FlashMessageType.Information,
            Title = "Information",
            Text = "Here is some important information.",
            Dismissible = true
        };

        private FlashMessage TestMessage2 = new FlashMessage
        {
            MessageType = FlashMessageType.Success,
            Title = "Successful operation",
            Text = "Here is some instructions on what to do next.",
            Dismissible = false
        };

        private FlashMessage TestMessage3 = new FlashMessage
        {
            MessageType = FlashMessageType.Dark,
            Title = "Random Title",
            Text = "Here is some random information to pass on to user.",
            Dismissible = true
        };

        [Fact]
        public async Task FlashMessageStoreReadTest()
        {
            string json = @"
                [
                    {
                        ""Key"": ""Message1"",
                        ""MessageType"": ""Information"",
                        ""Title"": ""Information"",
                        ""Text"": ""Here is some important information."",
                        ""Dismissible"": true
                    },
                    {
                        ""Key"": ""Message2"",
                        ""MessageType"": ""Success"",
                        ""Title"": ""Successful operation"",
                        ""Text"": ""Here is some instructions on what to do next."",
                        ""Dismissible"": false
                    }
                ]
            ";

            FileStream fs = new FileStream("./FlashMessageStoreReadTest.json", FileMode.Create);
            StreamWriter writer = new StreamWriter(fs);
            await writer.WriteAsync(json);
            await writer.FlushAsync();
            writer.Close();

            FlashMessageStore messageStore = new FlashMessageStore(new string[] { "./FlashMessageStoreReadTest.json" }, cacheMessages: false);

            Assert.True(AreEqual(messageStore["Message1"], TestMessage1));
            Assert.True(AreEqual(messageStore["Message2"], TestMessage2));
        }
        
        [Fact]
        public async Task FlashMessageStoreWriteTest()
        {
            FileStream fs = new FileStream("./FlashMessageStoreWriteTest.json", FileMode.Create);
            string[] keys = new string[] { "Message1", "Message2", "Message3" };
            FlashMessage[] messages = new FlashMessage[] { TestMessage1, TestMessage2, TestMessage3 };
            await FlashMessageStore.WriteFlashMessagesToStreamAsync(fs, keys, messages);
 
            FlashMessageStore messageStore = new FlashMessageStore(new string[] { "./FlashMessageStoreWriteTest.json" });
            FlashMessage msg = await messageStore.FindAsyc("Message1");
            Assert.True(AreEqual(msg, TestMessage1));
            msg = await messageStore.FindAsyc("Message2");
            Assert.True(AreEqual(msg, TestMessage2));
            msg = await messageStore.FindAsyc("Message3");
            Assert.True(AreEqual(msg, TestMessage3));
        }

        [Fact]
        public async Task FlashMessageStoreUncachedTest()
        {
            string storeFilename = "./FlashMessageStoreUncachedTest.json";
            List<FlashMessage> messages = new List<FlashMessage>(new FlashMessage[] { TestMessage1, TestMessage2 });
            List<string> keys = new List<string>(new string[] { "TestMessage1", "TestMessage2" });
            FileStream fs = new FileStream(storeFilename, FileMode.Create);
            await FlashMessageStore.WriteFlashMessagesToStreamAsync(fs, keys, messages);

            FlashMessageStore messageStore = new FlashMessageStore(storeFilename, cacheMessages: false);

            // Append a message to the store, after the message store has been initialised
            messages.Add(TestMessage3);
            keys.Add("TestMessage3");
            fs = new FileStream(storeFilename, FileMode.Create);
            await FlashMessageStore.WriteFlashMessagesToStreamAsync(fs, keys, messages);

            // Pass if an exception is not generated when trying to access the appended message
            FlashMessage appendedMessage;
            try { appendedMessage = await messageStore.FindAsyc("TestMessage3"); }
            catch (KeyNotFoundException)
            {
                Assert.True(false, "Appending new FlashMessage to file, after initialising the store, should still be found in uncached store.");
                return;
            }
            Assert.True(AreEqual(appendedMessage, TestMessage3));
        }



        private static bool AreEqual(FlashMessage msg1, FlashMessage msg2)
        {
            return (
                (msg1.MessageType == msg2.MessageType) &&
                (msg1.Title == msg2.Title) &&
                (msg1.Text == msg2.Text) &&
                (msg1.Dismissible == msg2.Dismissible));
        }
    }
}