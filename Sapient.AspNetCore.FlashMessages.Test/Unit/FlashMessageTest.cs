using System;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Sapient.AspNetCore.FlashMessages;
using Xunit;

namespace Sapient.AspNetCore.FlashMessages.Test.Unit
{
    public class FlashMessageTest
    {
        [Fact]
        public void FlashMessageConstructorWithoutParameter()
        {
            FlashMessage msg = new FlashMessage();
            Assert.True(msg.MessageType == FlashMessageType.Information, 
                "FlashMessage should have default message type of FlashMessageType.Information");
            Assert.True(String.IsNullOrEmpty(msg.Title),
                "FlashMessage should have empty string as default title");
            Assert.True(String.IsNullOrEmpty(msg.Text),
                "FlashMessageshould have empty string as default text");
            Assert.False(msg.Dismissible,
                "FlashMessage should not be dismissible by default");            
        }

        [Fact]
        public void FlashMessageConstructorWithTypeParameter()
        {
            FlashMessage msg = new FlashMessage(FlashMessageType.Success);
            Assert.True(msg.MessageType == FlashMessageType.Success, 
                "FlashMessage should not be default value when provided in constructor");
            Assert.True(String.IsNullOrEmpty(msg.Title),
                "FlashMessage should have empty string as default title");
            Assert.True(String.IsNullOrEmpty(msg.Text),
                "FlashMessage should have empty string as default text");
            Assert.False(msg.Dismissible,
                "FlashMessage should not be dismissible by default");

            msg = new FlashMessage(FlashMessageType.Danger, "Error", "An error has occurred", true);
            Assert.True(msg.MessageType == FlashMessageType.Danger, 
                "FlashMessage should not be default value when provided in constructor");
            Assert.False(String.IsNullOrEmpty(msg.Title),
                "FlashMessage should update title when provided in constructor");
            Assert.False(String.IsNullOrEmpty(msg.Text),
                "FlashMessage should not be empty when provided in constructor");
            Assert.True(msg.Dismissible,
                "FlashMessage should not be dismissible when specified as dismissible in constructor");
        }

        [Fact]
        public void FlashMessageConstructorWithStringMessageType()
        {
            FlashMessage badMessage;
            Assert.Throws<ArgumentException>(
                () => badMessage = new FlashMessage("BadMessageType"));
            
            FlashMessage msg = new FlashMessage("Primary");
            Assert.True(msg.MessageType == FlashMessageType.Primary,
                "FlashMessage should correcty convert string MessageType to associated enumerated value");
            Assert.True(String.IsNullOrEmpty(msg.Title),
                "FlashMessage should update title when provided in constructor");
            Assert.True(String.IsNullOrEmpty(msg.Text),
                "FlashMessage should be null when provided in constructor");
            Assert.False(msg.Dismissible,
                "FlashMessage should not be dismissible when specified as dismissible in constructor");

            msg = new FlashMessage("primary", "Error", "An error has occurred", true);
            Assert.True(msg.MessageType == FlashMessageType.Primary,
                "FlashMessage constructor should correctly parse lower case version of message type");
            Assert.False(String.IsNullOrEmpty(msg.Title),
                "FlashMessage should update title when provided in constructor");
            Assert.False(String.IsNullOrEmpty(msg.Text),
                "FlashMessage should not be empty when provided in constructor");
            Assert.True(msg.Dismissible,
                "FlashMessage should not be dismissible when specified as dismissible in constructor");            
        }

        [Fact]
        public void FlashMessageMessageTypeProperty()
        {
            FlashMessage msg = new FlashMessage();
            msg.MessageType = FlashMessageType.Warning;
            Assert.True(msg.MessageType == FlashMessageType.Warning,
                "Should set FlashMessageType when value is updated");
        }
        
        [Fact]
        public void FlashMessageTitleProperty()
        {
            FlashMessage msg = new FlashMessage();
            msg.Title = "Error";
            Assert.True(String.Equals("Error", msg.Title),
                "FlashMessage Title property should update after being set");
        }

        [Fact]
        public void FlashMessageTextProperty()
        {
            FlashMessage msg = new FlashMessage();
            msg.Text = "Some error message.";
            Assert.True(String.Equals("Some error message.", msg.Text),
                "FlashMessage Text property should update after being set");
        }

        [Fact]
        public void FlashMessageDismissibleProperty()
        {
            FlashMessage msg = new FlashMessage();
            msg.Dismissible = true;
            Assert.True(msg.Dismissible == true, 
                "FlashMessage Dismissible property should update after being set");
        }
    }
}