using System;
using Xunit;

namespace Sapient.AspNetCore.FlashMessages.Test.Unit
{
    public class FlashMessageTypeTest
    {
        [Theory]
        [InlineData(FlashMessageType.Danger, "4.0.2", "alert alert-danger")]
        [InlineData(FlashMessageType.Primary, "4.0.2", "alert alert-primary")]
        [InlineData(FlashMessageType.Primary, "3.3.7", "alert alert-info")]
        public void BootstrapClassesTest(FlashMessageType flashMessageType, string version, string htmlClass)
        {
            FlashMessage msg = new FlashMessage(flashMessageType, "Test", "This is a test.", false);
            Assert.True(msg.MessageType.GetBootstrapClass(version: version) == htmlClass,
                String.Format("FlashMessage of Type {0}, Version {1} should be {2}. Code returned {3}", 
                new object[] { flashMessageType, version, htmlClass, msg.MessageType.GetBootstrapClass() }));
        }

        [Fact]
        public void BootstrapClassesDefaultVersionTest()
        {
            FlashMessageType messageType = FlashMessageType.Dark;
            Assert.True(
                messageType.GetBootstrapClass() == "alert alert-dark",
                "GetBootstrapClass should return appropriate class for latest version of bootstrap when version unspecified"
            );
        }

        [Fact]
        public void BootstrapClassesExceptionsTest()
        {
            FlashMessage msg = new FlashMessage();
            Assert.Throws<ArgumentException>(
                () => msg.MessageType.GetBootstrapClass(version: "A"));
            Assert.Throws<ArgumentException>(
                () => msg.MessageType.GetBootstrapClass(version: "A.A"));
        }
    }
}