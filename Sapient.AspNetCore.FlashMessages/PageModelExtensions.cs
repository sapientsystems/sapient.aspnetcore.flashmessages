using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;

namespace Sapient.AspNetCore.FlashMessages
{
    public static class PageModelExtensions
    {
        public static void AddFlashMessage(this PageModel pageModel, FlashMessage flashMessage)
        {
            if (pageModel.TempData["FlashMessages"] == null)
            {
                pageModel.TempData.Put<FlashMessage[]>(
                    "FlashMessages",
                    new FlashMessage[] { flashMessage });
            }
            else
            {
                FlashMessage[] existingMessages = pageModel.TempData.Get<FlashMessage[]>("FlashMessages");
                List<FlashMessage> messages = new List<FlashMessage>(existingMessages);
                messages.Add(flashMessage);
                pageModel.TempData.Put<FlashMessage[]>("FlashMessages", messages.ToArray());
            }
        }

        public static bool HasFlashMessages(this PageModel pageModel)
        {
            return pageModel.TempData.HasFlashMessages();
        }

        public static bool HasFlashMessages(this ITempDataDictionary tempDataDictionary)
        {
            if (tempDataDictionary.ContainsKey("FlashMessages"))
            {
                try
                {
                    FlashMessage[] flashMessages = tempDataDictionary.Get<FlashMessage[]>("FlashMessages");
                    if (flashMessages != null)
                        return (flashMessages.Length > 0);
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }

        public static FlashMessage[] GetFlashMessages(this PageModel pageModel)
        {
            return pageModel.TempData.GetFlashMessages();

        }

        public static FlashMessage[] GetFlashMessages(this ITempDataDictionary tempDataDictionary)
        {
            if (!tempDataDictionary.ContainsKey("FlashMessages"))
                return new FlashMessage[] {};
            else
                return tempDataDictionary.Get<FlashMessage[]>("FlashMessages");
        }

        public static void Put<T>(this ITempDataDictionary tempDataDictionary, string key, T value) where T : class
        {
            tempDataDictionary[key] = JsonConvert.SerializeObject(value);
        }

        public static T Get<T>(this ITempDataDictionary tempDataDictionary, string key) where T : class
        {
            object o;
            bool success = tempDataDictionary.TryGetValue(key, out o);

            return success ? JsonConvert.DeserializeObject<T>((string)o) : null;
        }        
    }
}