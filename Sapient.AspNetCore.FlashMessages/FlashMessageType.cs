using System;

namespace Sapient.AspNetCore.FlashMessages
{
    /// <summary>
    /// An enum type to represent the type of a flash message being generated
    /// </summary>
    /// <remarks>
    /// The available values are those in the latest version of bootstrap (v4.0.0-beta2)
    /// </remarks>
    public enum FlashMessageType : byte
    {
        Primary = 1,
        Secondary = 2,
        Success = 3,
        Danger = 4,
        Warning = 5,
        Information = 6,
        Light = 7,
        Dark = 8
    }
}