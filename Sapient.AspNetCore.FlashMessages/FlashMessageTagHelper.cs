using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Sapient.AspNetCore.FlashMessages
{
    [HtmlTargetElement("flash", Attributes = "asp-message-type, asp-message")]
    public class FlashMessageTagHelper : TagHelper
    {
        private bool _Dismissible = false;
        private string _BootstrapVersion = string.Empty;

        [HtmlAttributeName("asp-message-type")]
        public FlashMessageType MessageType { get; set; }

        [HtmlAttributeName("asp-title")]
        public string Title { get; set; }

        [HtmlAttributeName("asp-message")]
        public string Message { get; set; }

        [HtmlAttributeName("asp-dismiss")]
        public bool Dismissible
        {
            get { return this._Dismissible; }
            set { this._Dismissible = value; }
        }

        [HtmlAttributeName("asp-bootstrap-version")]
        public string BootstrapVersion
        {
            get 
            {
                if (string.IsNullOrEmpty(this._BootstrapVersion))
                    return "4.0.0-beta.3";
                else
                    return this._BootstrapVersion;
            }
            set
            {
                this._BootstrapVersion = value;
            }
        }

        private string BootstrapClass
        {
            get
            {
                return this.Dismissible ?
                    this.MessageType.GetBootstrapClass(this.BootstrapVersion) : 
                    this.MessageType.GetBootstrapClass(this._BootstrapVersion) + " alert-dismissible";
            }
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "div";
            output.TagMode = TagMode.StartTagAndEndTag;
            output.Attributes.SetAttribute("class", this.BootstrapClass);
            
            if (this.Dismissible)
            {
                output.Content.AppendHtml(
                    @"<button type=""button"" class=""close"" data-dismiss=""alert"" aria-label=""Close"">
                        <span aria-hidden=""true"">&times;</span>
                    </button>"
                );
            }
            if (!string.IsNullOrEmpty(this.Title))
            {
                output.Content.AppendHtml(
                    $"<strong>{this.Title} </strong>"
                );
            }
            output.Content.AppendHtml(this.Message);
        }
    }
}