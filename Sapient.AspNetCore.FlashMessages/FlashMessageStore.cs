using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Sapient.AspNetCore.FlashMessages
{
    public class FlashMessageStore
    {
        private Dictionary<string, FlashMessage> _MessageStore;
        private bool _CacheMessages = false;
        private bool _SkipReadErrors = false;
        private bool _IgnoreDuplicateKeys = true;
        private ICollection<string> _MessageStorePaths;

        public bool CacheMessages
        {
            get { return this._CacheMessages; }
            set { this._CacheMessages = value; }
        }

        public bool SkipReadErrors
        {
            get { return this._SkipReadErrors; }
            set { this._SkipReadErrors = value; }
        }

        public bool IgnoreDuplicateKeys
        {
            get { return this._IgnoreDuplicateKeys; }
            set { this._IgnoreDuplicateKeys = value; }
        }

        public FlashMessage this[string key]
        {
            get
            {
                return this.FindAsyc(key).GetAwaiter().GetResult();
            }
        }


        public FlashMessageStore(string messageStorePath, bool cacheMessages = false) :
            this(new string[] { messageStorePath }, cacheMessages)
        {
        }


        public FlashMessageStore(ICollection<string> messageStorePaths, bool cacheMessages = false)
        {
            if (messageStorePaths == null)
                throw new ArgumentException("Message storage paths may not be null");
            this._MessageStorePaths = messageStorePaths;
            this.CacheMessages = cacheMessages;
        }

        
        public async Task<FlashMessage> FindAsyc(string key)
        {
            if (this.CacheMessages)
            {
                if (this._MessageStore.ContainsKey(key))
                    return this._MessageStore[key];
            }

            await this.UpdateMessageStore();
            if (this._MessageStore.ContainsKey(key))
                return this._MessageStore[key];
            else
                throw new KeyNotFoundException(
                    String.Format("Key {0} not found in FlashMessageStore.", key));
        }

        public async Task<FlashMessage[]> GetAllAsync()
        {
            if (!this.CacheMessages)
                await this.UpdateMessageStore();                

            FlashMessage[] outArray;
            outArray = new FlashMessage[this._MessageStore.Values.Count];
            _MessageStore.Values.CopyTo(outArray, 0);
            return outArray;
        }

        protected async Task UpdateMessageStore()
        {
            this._MessageStore = new Dictionary<string, FlashMessage>();

            foreach (string messageStorePath in this._MessageStorePaths)
            {
                FileStream fileStream;
                try { fileStream = new FileStream(messageStorePath, FileMode.Open); }
                catch (Exception exc)
                {
                    if (!this.SkipReadErrors)
                        throw new IOException(
                            String.Format("Error opening file {0}. System returned error of type {1}", messageStorePath, exc.GetType().ToString()),
                            exc);
                    else
                        break;
                }

                string messagesJson;
                try
                {
                    StreamReader reader = new StreamReader(fileStream);
                    messagesJson = await reader.ReadToEndAsync();
                    reader.Close();
                }
                catch (Exception exc)
                {
                    if (!this.SkipReadErrors)
                    {
                        fileStream.Close();
                        throw new IOException(
                            String.Format("Error reading stream from {0}. System returned error of type {1}", messageStorePath, exc.GetType().ToString()),
                            exc);
                    }
                    else
                        break;                    
                }

                List<StoredFlashMessage> flashMessages = new List<StoredFlashMessage>();
                try
                {
                    flashMessages = JsonConvert.DeserializeObject<List<StoredFlashMessage>>(messagesJson);
                }
                catch (Exception exc)
                {
                    if (!this.SkipReadErrors)
                        throw new JsonSerializationException(
                            String.Format("Unable to read stream from {0}. Incorrect file format", messageStorePath),
                            exc);
                    else
                        break;
                }

                foreach (StoredFlashMessage storedFlashMessage in flashMessages)
                {
                    if (this._MessageStore.ContainsKey(storedFlashMessage.Key))
                    {
                        if (!this.IgnoreDuplicateKeys)
                            throw new ApplicationException(
                                String.Format("Duplicate key {0} in FlashMessageStore.", storedFlashMessage.Key));
                        else
                            break;
                    }
                    this._MessageStore.Add(storedFlashMessage.Key, storedFlashMessage.ToFlashMessage());
                }
            }
        }


        public async static Task WriteFlashMessagesToStreamAsync(Stream s, ICollection<string> keys, ICollection<FlashMessage> flashMessages)
        {
            if (keys.Count != flashMessages.Count)
                throw new ArgumentException(
                    String.Format("Unable to write messages. Mismatch between number of keys ({0}) and messages ({1})", keys.Count, flashMessages.Count)
                );
            
            Dictionary<string, FlashMessage> messageDictionary = new Dictionary<string, FlashMessage>();
            List<string> keyList = new List<string>(keys);
            List<FlashMessage> flashMessageList = new List<FlashMessage>(flashMessages);
            for (int i = 0; i < keyList.Count; i++)
            {
                messageDictionary.Add(keyList[i], flashMessageList[i]);
            }
            await FlashMessageStore.WriteFlashMessagesToStreamAsync(s, messageDictionary);
        }

        public static async Task WriteFlashMessagesToStreamAsync(Stream s, IDictionary<string, FlashMessage> messageDictionary)
        {
            if (messageDictionary == null)
                messageDictionary = new Dictionary<string, FlashMessage>();
            
            List<StoredFlashMessage> messagesToSerialize = new List<StoredFlashMessage>();
            foreach (KeyValuePair<string, FlashMessage> kvp in messageDictionary)
            {
                messagesToSerialize.Add(new StoredFlashMessage(kvp.Key, kvp.Value));
            }
            string serializedMessages = Newtonsoft.Json.JsonConvert.SerializeObject(messagesToSerialize);
            StreamWriter writer = new StreamWriter(s);
            await writer.WriteAsync(serializedMessages);
            await writer.FlushAsync();
            writer.Close();
        }


        [JsonObject(MemberSerialization.OptIn)]       
        protected class StoredFlashMessage
        {
            [JsonProperty]
            public string Key { get; set; }

            [JsonProperty]
            [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
            public FlashMessageType MessageType { get; set; }

            [JsonProperty]
            public string Title { get; set; }

            [JsonProperty]
            public string Text { get; set; }

            [JsonProperty]
            public bool Dismissible { get; set; }
            
            public StoredFlashMessage() { }

            public StoredFlashMessage(string key, FlashMessage flashMessage)
            {
                this.Key = key;
                this.MessageType = flashMessage.MessageType;
                this.Title = flashMessage.Title;
                this.Text = flashMessage.Text;
                this.Dismissible = flashMessage.Dismissible;
            }

            public KeyValuePair<string, FlashMessage> ToKeyValuePair()
            {
                return new KeyValuePair<string, FlashMessage>(
                    this.Key,
                    new FlashMessage {
                        MessageType = this.MessageType,
                        Title = this.Title,
                        Text = this.Text,
                        Dismissible = this.Dismissible
                    }
                );
            }

            public FlashMessage ToFlashMessage()
            {
                return new FlashMessage {
                    MessageType = this.MessageType,
                    Title = this.Title,
                    Text = this.Text,
                    Dismissible = this.Dismissible
                };
            }
        }
    }
}