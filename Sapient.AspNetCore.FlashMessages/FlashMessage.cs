using System;

namespace Sapient.AspNetCore.FlashMessages
{
    /// <summary>
    /// A class to represent a flash message to be displayed to a user on an ASP.NET Core Razor
    /// Page.
    /// </summary>
    public class FlashMessage
    {
        private FlashMessageType _MessageType = FlashMessageType.Information;
        private string _Title = string.Empty;
        private string _Text = string.Empty;
        private bool _Dismissible = false;

        /// <summary>
        /// The type of the flash message.
        /// </summary>
        public FlashMessageType MessageType
        {
            get { return this._MessageType; }
            set { this._MessageType = value; }
        }

        /// <summary>
        /// The (short) title of the flash message.
        /// </summary>
        /// <remarks>
        /// This value should be a very small number of words, which are emphasized and displayed
        /// first in the visual representation of the message.
        /// </remarks>
        public string Title
        {
            get { return this._Title; }
            set { this._Title = value; }
        }

        /// <summary>
        /// The explanatory text to be displayed as part of the flash message.
        /// </summary>
        public string Text
        {
            get { return this._Text; }
            set { this._Text = value; }
        }

        /// <summary>
        /// Whether the user is able to dismiss the alert from the screen.
        /// </summary>
        public bool Dismissible
        {
            get { return this._Dismissible; }
            set { this._Dismissible = value; }
        }


        public FlashMessage()
        {
        }

        public FlashMessage(
            FlashMessageType messageType,
            string title = null,
            string text = null,
            bool dismissible = false)
        {
            this.MessageType = messageType;
            this.Title = title;
            this.Text = text;
            this.Dismissible = dismissible;
        }

        public FlashMessage(
            string messageTypeString,
            string title = null,
            string text = null,
            bool dismissible = false)
        {
            FlashMessageType messageType;
            bool success = Enum.TryParse<FlashMessageType>(messageTypeString, true, out messageType);
            if (!success)
                throw new ArgumentException(String.Format("Unrecognized FlashMassageType: {0}", messageTypeString));
            this.MessageType = messageType;
            this.Title = title;
            this.Text = text;
            this.Dismissible = dismissible;
        }
    }
}