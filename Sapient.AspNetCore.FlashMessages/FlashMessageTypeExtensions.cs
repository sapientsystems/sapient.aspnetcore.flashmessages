using System;

namespace Sapient.AspNetCore.FlashMessages
{
    /// <summary>
    /// Extension methods for the FlashMessageType enumerated type
    /// </summary>
    public static class FlashMessageTypeExtensions
    {
        public static readonly string DefaultBootstrapVersion = "4.0.0-beta3";

        /// <summary>
        /// Provides the equivalent bootstrap HTML class definitions for the value of the FlashMessageType
        /// </summary>
        public static string GetBootstrapClass(this FlashMessageType messageType, string version = null)
        {
            if (String.IsNullOrEmpty(version))
                version = DefaultBootstrapVersion;

            string[] subversions = version.Split(new char[] {'.'}, StringSplitOptions.RemoveEmptyEntries);
            if (subversions.Length < 1)
                throw new ArgumentException(
                    String.Format("Unrecognized Bootstrap version {0}.", version));

            int majorVersion;
            bool success = Int32.TryParse(subversions[0], out majorVersion);
            if (!success)
                throw new ArgumentException(String.Format("Unrecognized Bootstrap version {0}.", version));

            if (majorVersion >= 4)
            {
                switch (messageType)
                {
                    case FlashMessageType.Primary:
                        return "alert alert-primary";
                    case FlashMessageType.Secondary:
                        return "alert alert-secondary";
                    case FlashMessageType.Success:
                        return "alert alert-success";
                    case FlashMessageType.Danger:
                        return "alert alert-danger";
                    case FlashMessageType.Warning:
                        return "alert alert-warning";
                    case FlashMessageType.Information:
                        return "alert alert-info";
                    case FlashMessageType.Light:
                        return "alert alert-light";
                    case FlashMessageType.Dark:
                        return "alert alert-dark";
                    default:
                        return "alert alert-primary";
                }
            }
            else
            {
                switch (messageType)
                {
                    case FlashMessageType.Success:
                        return "alert alert-success";
                    case FlashMessageType.Information:
                        return "alert alert-info";
                    case FlashMessageType.Warning:
                        return "alert alert-warning";
                    case FlashMessageType.Danger:
                        return "alert alert-danger";
                    default:
                        return "alert alert-info";
                }

            }
        }
    }
}