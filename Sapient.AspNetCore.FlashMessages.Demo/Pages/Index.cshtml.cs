﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Sapient.AspNetCore.FlashMessages;

namespace Sapient.AspNetCore.FlashMessages.Demo.Pages
{
    public class IndexModel : PageModel
    {
        [BindProperty]
        public InputModel Input { get; set; }

        private FlashMessageStore FlashMessages { get; set; }


        public IndexModel(FlashMessageStore flashMessageStore)
        {
            this.FlashMessages = flashMessageStore;
        }


        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {

            if (!this.ModelState.IsValid)
            {
                this.AddFlashMessage(this.FlashMessages["FormError"]);
                return Page();
            }
            else
            {
                this.AddFlashMessage(new FlashMessage{
                    MessageType = FlashMessageType.Primary,
                    Text = String.Format("The answer is {0} + {1} = {2}", this.Input.Value1, this.Input.Value2, 
                        this.Input.Value1 + this.Input.Value2)
                });
                return this.Redirect("/Index");
            }
        }

        public class InputModel
        {
            [Required]
            [Range(0, 10)]
            [Display(Name = "Value 1")]
            public int Value1 { get; set; }

            [Required]
            [Range(0, 10)]
            [Display(Name = "Value 2")]
            public int Value2 { get; set; }
        }
    }
}
