using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Sapient.AspNetCore.FlashMessages;

namespace Sapient.AspNetCore.FlashMessages.Demo.Pages
{
    public class FlashStoreListModel : PageModel
    {
        public FlashMessageStore FlashMessages { get; set; }

        public string BootstrapVersion { get; set; }

        public int BootstrapMajorVersion
        {
            get
            {
                if (String.IsNullOrEmpty(this.BootstrapVersion))
                    throw new InvalidOperationException(
                        String.Format("Unable to determine major version for BootrapVersion: {0}", this.BootstrapVersion));

                string[] subversions = this.BootstrapVersion.Split(new char[] {'.'}, StringSplitOptions.RemoveEmptyEntries);
                if (subversions.Length < 1)
                    throw new InvalidOperationException(
                        String.Format("Unrecognized Bootstrap version {0}.", this.BootstrapVersion));

                int majorVersion;
                bool success = Int32.TryParse(subversions[0], out majorVersion);
                if (!success)
                    throw new InvalidOperationException(
                        String.Format("Unrecognized Bootstrap version {0}.", this.BootstrapVersion));

                return majorVersion;
            }
        }

        
        public FlashStoreListModel(FlashMessageStore flashMessageStore)
        {
            this.FlashMessages = flashMessageStore;
        }


        public void OnGet(string bootstrapVersion = "3.3.7")
        {
            this.BootstrapVersion = bootstrapVersion;
        }
    }
}