# Sapient.AspNetCore.FlashMessages

This package provides a number of classes to provide flash message functionality (similar to that
in Ruby on Rails) to ASP.NET Core Razor page applications. These messages allow for one instance
of a PageModel to provide structured objects (as FlashMessage objects) to a subsequent PageModel.
These messages can subsequenty be displayed on the UI, optionally using the FlashMessageTagHelper
to display a Bootstrap-style alert.

## Getting Started

### Installing
The simplest method to install this package into your ASP.NET Core Razor Page application is to
obtain using the NuGet Package Manager:
Package Manager:
```
PM> Install-Package Sapient.AspNetCore.FlashMessages
```

.NET CLI
```
> dotnet add package Sapient.AspNetCore.FlashMessages
```
### FlashMessages in Razor Page PageModel
To start using FlashMessages in your application, add the following using directive to your PageModel
```
using System;
...
using Sapient.AspNetCore.FlashMessages;

namespace FlashMessageExample
{
    public class FlashExample : PageModel
    {
        ...

        public void OnGet()
        {
            AddFlashMessage(new FlashMessage {
                MessageType = FlashMessageType.Success,
                Title = "Success!",
                Text = "The operation completed successfully.",
                Dismissible = true
            });

        }

        public IActionResult OnPost()
        {
            AddFlashMessage(new FlashMessage {
                MessageType = FlashMessageType.Danger,
                Title = "Error!",
                Text = "There was an error performing the requested action.",
                Dismissible = false
            });
            return Redirect("./AnotherPage");
        }
    }
}
```

### Displaying FlashMessages in Razor View
From a RazorPage view, the FlashMessages can be obtained using the ```Get<T>``` extension method for the
```ITempDataDictionary``` as shown in the example below. This example will is from the bundled demonstration, 
and displays all of the FlashMessages as Bootstrap as a partial:

```
@using Sapient.AspNetCore.FlashMessages;

@if (TempData.HasFlashMessages())
{
    FlashMessage[] messages = TempData.Get<FlashMessage[]>("FlashMessages");
    @foreach (var message in messages)
    {
        <flash asp-message-type="@message.MessageType"
            asp-title="@message.Title",
            asp-message="@message.Text",
            asp-dismiss="@message.Dismissible"
            asp-bootstrap-version="3.3.7" />
    }
}
```

FlashMessages are stored in the ```TempData``` ```ITempDataDictionary``` in the Razor page, using the "FlashMessages"
index, in a json format which is deserialized by the extension methods provided. The above example uses the 
```FlashMessageTagHelper```,  which provides a custom tag to generate Bootstrap-style alerts from the FlashMessage contents.

### Using the FlashMessageStore
The FlashMessageStore class provides a facility to store FlashMessages in a JSON formatted database, which can then be
accessed from Razor PageModel.

#### Setup & Configuration of FlashMessageStore
To setup and configure the ```FlashModelStore```, an singleton instance will usually be created for the WebHost, configured
in the ```Startup.cs``` file. The example code creates an instance of a FlashMessageStore, which uses a database stored at
```./FlashMessages.json``` as a store for FlashMessages.

Startup.cs
```
...
using Sapient.AspNetCore.FlashMessages;

namespace FlashMessageExample
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            FlashMessageStore flashMessageStore = new FlashMessageStore(
                "./FlashMessages.json", cacheMessages: false);
            services.AddSingleton<FlashMessageStore>(flashMessageStore);
        }

        ...
    }
}
```

FlashMessages.json
```
[
    {
        "Key": "FormError",
        "MessageType": "Danger",
        "Title": "Error!",
        "Text": "The form contains errors. Please correct and resubmit",
        "Dismissible": false
    },
    {
        "Key": "OperationSuccess",
        "MessageType": "Success",
        "Title": "Success.",
        "Text": "The requested operation completed successfully",
        "Dismissible": true
    }
]
```

#### Accessing FlashMessageStore from a Razor Page
With the ```FlashMessageStore``` registered in the startup ```IServiceCollection```, the message store is accessed using
dependency injection in the Razor PageModel. This is shown in the example code, where the ```IndexModel``` includes an 
instance of FlashMessageStore, which is set in the constructor.

The messages are accessed using their Key value in the database.

```
...
using Sapient.AspNetCore.FlashMessages;

namespace FlashMessageExample
{
    public class IndexModel : PageModel
    {
        ...

        private FlashMessageStore FlashMessages { get; set; }


        public IndexModel(FlashMessageStore flashMessageStore)
        {
            FlashMessages = flashMessageStore;
        }

        ...

        public IActionResult OnPost()
        {

            if (!ModelState.IsValid)
            {
                AddFlashMessage(this.FlashMessages["FormError"]);
                return Page();
            }
            ...
        }
    }
}
```

## Running the tests
The project includes a number of automated tests that verify the basic functionality of some of the
classes. Note that this does not yet include the ```PageModel``` & ```ITempDataDictionary``` 
extension methods or the ```FlashMessageTagHelper``` classes. These will be added at a later date.

To run the tests using the .NET CLI, simply enter the following command from the solution directory
```
> dotnet test
```

## Contributing
This module has very limited functionality, as it was developed for a specific website. Sapient Systems
will continue development, but would accept any feedback and/or improvements suggested. Please 
contact <sapientsystems@protonmail.com> to discuss your ideas, or get access to the code repository to 
provide your own contributions.

## Authors
This module was developed by Matthew Kitchin (<kitchin_ms@hotmail.com>)

## License
This project is licensed under the MIT License.
